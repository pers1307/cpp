<?php

namespace AdminBundle\Controller;

use FOS\RestBundle\Controller\AbstractFOSRestController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;

class DashboardController extends AbstractFOSRestController
{
    /**
     * @Route("/admin", name="admin")
     * @return Response
     */
    public function indexAction()
    {
//        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        return $this->render('@Admin/Dashboard/dashboard.html.twig');
    }
}
