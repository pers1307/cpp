<?php

namespace AdminBundle\Controller\Matching;

use AdminBundle\Forms\LoadProductListFormType;
use AdminBundle\Forms\Model\LoadProductListModel;
use AdminBundle\Service\FileMatchingService;
use AdminBundle\Service\FileParseService;
use AdminBundle\Service\MakeMatchFileService;
use CoreBundle\Entity\FileMatching;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\Request;
use Knp\Component\Pager\PaginatorInterface;

class FileMatchingController extends Controller
{
    /**
     * @Route("/admin/matching/file", name="file_matching")
     *
     * @param Request $request
     * @param PaginatorInterface $paginator
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request, PaginatorInterface $paginator)
    {
        $loadProductListModel = new LoadProductListModel();
        $form = $this->createForm(LoadProductListFormType::class, $loadProductListModel);
        $form->handleRequest($request);

        /** @var FileParseService $fileParseService */
        $fileParseService = $this->container->get('file_parse_service');

        if (
            $fileParseService->parseFileFromRequest($request)
        ) {
            /** @var FileMatchingService $fileMatchingService */
            $fileMatchingService = $this->container->get('file_matching_service');
            $fileMatchingService->startMatch();
        }

        $fileMatchingAllQuery = $this
            ->getDoctrine()
            ->getRepository('CoreBundle:FileMatching')
            ->getAllQuery();

        $fileMatchings = $paginator->paginate(
            $fileMatchingAllQuery,
            $request->query->getInt('page', 1),
            5
        );

        return $this->render('@Admin/Matching/file.html.twig', [
            'add_file_form' => $form->createView(),
            'fileMatchings' => $fileMatchings,
        ]);
    }

    /**
     * @Route("/admin/matching/approve/{id}", name="approve_match")
     *
     * @param FileMatching $fileMatching
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function AddImageAction(FileMatching $fileMatching, Request $request)
    {
        $saveImageIds = $request->query->all();

        /** @var FileMatchingService $fileMatchingService */
        $fileMatchingService = $this->container->get('file_matching_service');

        $fileMatchingService->saveImageInFileMatch($fileMatching, $saveImageIds);

        $referer = $request->headers->get('referer');
        return $this->redirect($referer, 301);
    }

    /**
     * @Route("/admin/matching/download", name="download_file")
     */
    public function DownloadResultFileAction()
    {
        /** @var MakeMatchFileService $makeFileMatchService */
        $makeFileMatchService = $this->container->get('make_match_file_service');
        $resultPathToFile = $makeFileMatchService->makeFile();

        $file = new File($resultPathToFile);
        return $this->file($file);
    }
}