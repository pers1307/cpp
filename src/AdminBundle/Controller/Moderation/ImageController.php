<?php

namespace AdminBundle\Controller\Moderation;

use CoreBundle\Entity\Image;
use CoreBundle\Entity\ImageModeration;
use CoreBundle\Entity\Product;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Form\Tests\Fixtures\Type;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Knp\Component\Pager\PaginatorInterface;

use Swagger\Annotations as SWG;

class ImageController extends AbstractFOSRestController
{
    /**
     * @Route("/admin/moderation/image", name="moderation_image")
     * @param Request $request
     * @return Response
     */
    public function AdminModerationAction(Request $request, PaginatorInterface $paginator)
    {
        $products = $this
            ->getDoctrine()
            ->getRepository('CoreBundle:Product')
            ->findActive();

        // todo: зарефачить это потом!
        $allProducts = [];
        foreach ($products as $product) {
            $allProducts[] = $product->getImageModeration();
        }

        if (!empty($products)) {
            $pagination = $paginator->paginate(
                $allProducts, // сюда закинуть мой массив с продуктами
                $request->query->getInt('page', 1), 10
            );

            return $this->render('@Admin/Moderation/image.html.twig', [
                'products' => $pagination,
            ]);
        } else {
            return $this->render('@Admin/Moderation/image.html.twig');
        }
    }

    /**
     * @Route("/admin/moderation/image/remove/{id}", name="remove_image")
     * @param Product $product
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function RemoveImageModerationAction(Product $product)
    {
        // отклонить фото(не прошло модерацию)
        $productId = $product->getId();
        $em = $this->getDoctrine()->getManager();
        
        $imageModerations = $this->getDoctrine()->getRepository(ImageModeration::class)->findBy([
            'product' => $productId
        ]);

        foreach ($imageModerations as $imageModeration) {
            $dirNamePath = dirname(__DIR__, 3) . DIRECTORY_SEPARATOR . 'web'; // путь к папки web
            $pathImg = $dirNamePath . DIRECTORY_SEPARATOR . $imageModeration->getPictureUrl(); //полный путь к картинке
            $em->remove($imageModeration);

            if (file_exists($pathImg)) {
                unlink($pathImg);
            }
        }
        $em->flush();

        return $this->redirectToRoute('moderation_image');
    }

    /**
     * @Route("/admin/moderation/image/add/{id}", name="add_image")
     * @param Product $product
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function AddImageAction(Product $product)
    {
        /**
         * Продублируем логику
         */
        $productId = $product->getId();
        $repositoryModerImg = $this->getDoctrine()->getRepository(ImageModeration::class)->findBy(
            [
                'product' => $productId
            ]);
        $em = $this->getDoctrine()->getManager();

        foreach ($repositoryModerImg as $rep) {
            $data = new Image();
            $productImg = $rep->getProduct();
            $typeimage = $rep->getTypeimage();
            $pictureUrl = $rep->getPictureUrl();
            $size = $rep->getSize();
            $width = $rep->getWidth();
            $length = $rep->getLength();

            $data->setProduct($productImg);
            $data->setTypeimage($typeimage);
            $data->setPictureUrl($pictureUrl);
            $data->setSize($size);
            $data->setWidth($width);
            $data->setLength($length);

            $em->persist($data);
        }

        foreach ($repositoryModerImg as $moderImg) {
            $em->remove($moderImg);
        }
        $em->flush();

        return $this->redirectToRoute('moderation_image');
    }

    public function searchBarAction()
    {
        $form = $this->createFormBuilder(null)
            ->add('search', TextType::class, [
                'label' => false,
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'search',
            ])
            ->getForm();

        return $this->render('@Admin/Search/SearchBar.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/admin/moderation/image/search", name="handleSearch")
     *
     * @param Request $request
     * @param PaginatorInterface $paginator
     * @return Response
     */
    public function handleSearch(Request $request, PaginatorInterface $paginator)
    {
        $resultForm = $request->request->get('form')['search'];
        $products = $this
            ->getDoctrine()
            ->getRepository('CoreBundle:Product')
            ->findByName($resultForm);

        // todo: зарефачить это потом!
        $allProducts = [];
        foreach ($products as $product) {
            $allProducts[] = $product->getImageModeration();
        }

        if (!empty($products)) {
            $pagination = $paginator->paginate(
                $allProducts, // сюда закинуть мой массив с продуктами
                $request->query->getInt('page', 1), 10
            );

            return $this->render('@Admin/Moderation/image.html.twig', [
                'products' => $pagination,
            ]);
        } else {
            return $this->render('@Admin/Moderation/image.html.twig');
        }

    }
}
