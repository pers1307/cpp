<?php

namespace AdminBundle\Service;

use CoreBundle\Entity\FileMatching;
use CoreBundle\Exception\FileNotMoveException;
use CoreBundle\Exception\NotFoundRequireParameterException;
use CoreBundle\Exception\WrongFileExtensionException;
use CoreBundle\Service\TranslitHelper;
use Doctrine\ORM\EntityManagerInterface;
use PhpOffice\PhpSpreadsheet\IOFactory;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;

class FileParseService
{
    /**
     * @var EntityManagerInterface
     */
    protected $entityManager;

    /**
     * @var TranslitHelper
     */
    protected $translitHelper;

    /**
     * @var string
     */
    protected $uploadDirectory;

    /**
     * @var array
     */
    protected $allowFileExtensions;

    /**
     * @var string
     */
    protected $newTempFileName = 'lastFile';

    /**
     * FileParseService constructor
     *
     * @param $uploadDirectory
     * @param $allowFileExtensions
     * @param EntityManagerInterface $entityManager
     * @param TranslitHelper $translitHelper
     */
    public function __construct(
        $uploadDirectory,
        $allowFileExtensions,
        EntityManagerInterface $entityManager,
        TranslitHelper         $translitHelper
    ) {
        $this->uploadDirectory     = $uploadDirectory;
        $this->allowFileExtensions = $allowFileExtensions;
        $this->entityManager       = $entityManager;
        $this->translitHelper      = $translitHelper;
    }

    public function parseFileFromRequest(Request $request)
    {
        try {
            $this->checkParameters($request);
        } catch (NotFoundRequireParameterException $exception) {
            /**
             * todo: проверить существующий файл
             */
            return false;
        }

        /** @var UploadedFile $file */
        $file = $this->extractParametersFromRequest($request);

        $newFileName = $this->newTempFileName . '.' . $file->getClientOriginalExtension();
        $pathToFile = $this->uploadDirectory . '/' . $newFileName;

        if (
           !move_uploaded_file($file->getPathname(), $pathToFile)
        ) {
            throw new FileNotMoveException(
                'Fail move file to ' . $this->uploadDirectory . ' path'
            );
        }

        // todo: заложиться на то, что может уже быть файлик

        $this->parse($pathToFile);

        return true;
    }

    public function parse($pathToFile)
    {
        $result = $this->convertFileToArray($pathToFile);

        $this
            ->entityManager
            ->getRepository('CoreBundle:FileMatching')
            ->removeAll();

        foreach ($result as $row) {
            foreach ($row as $item) {
                $fileMatching = new FileMatching();
                $fileMatching->setTitle($item);

                $this->entityManager->persist($fileMatching);
            }
        }
        $this->entityManager->flush();
    }

    /**
     * @param string $pathToFile
     * @return array
     */
    protected function convertFileToArray($pathToFile)
    {
        $spreadsheet = IOFactory::load($pathToFile);
        $worksheet   = $spreadsheet->getActiveSheet();
        $rows = [];

        foreach ($worksheet->getRowIterator() AS $row) {
            $cellIterator = $row->getCellIterator();
            $cellIterator->setIterateOnlyExistingCells(FALSE);
            $cells = [];

            foreach ($cellIterator as $cell) {
                if (!is_null($cell->getValue())) {
                    $cells[] = $cell->getValue();
                }
            }

            $rows[] = $cells;
        }

        unset($spreadsheet);
        unset($worksheet);

        return $rows;
    }

    /**
     * @param Request $request
     * @return UploadedFile|null
     */
    protected function extractParametersFromRequest(Request $request)
    {
        if (!empty($request->files)) {
            $files = $request->files->all();

            if (!empty($files) && !empty($files['load_product_list_form'])) {
                return $files['load_product_list_form']['file'];
            }
        }

        return null;
    }

    /**
     * @param Request $request
     */
    protected function checkParameters(Request $request)
    {
        $file = $this->extractParametersFromRequest($request);

        if (is_null($file)) {
            throw new NotFoundRequireParameterException(
                'Not found file for upload parameter'
            );
        }

        if (!in_array($file->getClientOriginalExtension(), $this->allowFileExtensions)) {
            throw new WrongFileExtensionException(
                'File have wrong extension. Allow extension are '
                . implode(', ', $this->allowFileExtensions)
            );
        }
    }
}