<?php

namespace AdminBundle\Service;

use AdminBundle\Exception\FileMachingApprovedYetException;
use CoreBundle\Entity\FileMatching;
use CoreBundle\Entity\Image;
use CoreBundle\Entity\Product;
use Doctrine\ORM\EntityManagerInterface;

class FileMatchingService
{
    /**
     * @var EntityManagerInterface
     */
    protected $entityManager;

    /**
     * FileMatchingService constructor.
     *
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(
        EntityManagerInterface $entityManager
    ) {
        $this->entityManager = $entityManager;
    }

    public function startMatch()
    {
        $fileMatchings = $this
            ->entityManager
            ->getRepository('CoreBundle:FileMatching')
            ->findAll();

        /** @var FileMatching $fileMatching */
        foreach ($fileMatchings as $fileMatching) {
            // todo: оставим пока так, если искать каждые файлы,
            // то проблемы с производительностью

//            $itemArrayWords = explode(' ', $fileMatching->getTitle());
//            array_unshift($itemArrayWords, $fileMatching->getTitle());

            $products = $this
                ->entityManager
                ->getRepository('CoreBundle:Product')
                ->findByNameFromWordArray([$fileMatching->getTitle()]);

            $resultImages = [];
            /** @var Product $product */
            foreach ($products as $product) {
                $images = $product->getImages();
                $images = $images->getValues();

                $resultImages = array_merge($resultImages, $images);
            }
            $fileMatching->setImages($resultImages);

            $this->entityManager->persist($fileMatching);
        }
        $this->entityManager->flush();
    }

    public function saveImageInFileMatch(FileMatching $fileMatching, $saveImageIds)
    {
        if ($fileMatching->getApproved() == true) {
            throw new FileMachingApprovedYetException(
                'File matching approved yet'
            );
        }

        $images = $fileMatching->getImages();
        $images = $images->getValues();

        /** @var Image $image */
        foreach ($images as $image) {
            $find = false;
            foreach ($saveImageIds as $saveImageId) {
                if ($image->getId() == $saveImageId) {
                    $find = true;
                }
            }

            if ($find == false) {
                $fileMatching->removeImage($image);
            }
        }
        $fileMatching->setApproved(true);

        $this->entityManager->persist($fileMatching);
        $this->entityManager->flush();
    }
}