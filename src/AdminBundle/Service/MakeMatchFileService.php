<?php

namespace AdminBundle\Service;

use CoreBundle\Entity\FileMatching;
use CoreBundle\Entity\Image;
use Doctrine\ORM\EntityManagerInterface;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class MakeMatchFileService
{
    /**
     * @var EntityManagerInterface
     */
    protected $entityManager;

    /**
     * @var string
     */
    protected $uploadDirectory;

    /**
     * @var string
     */
    protected $newTempFileName = 'matchFile';

    /**
     * FileParseService constructor
     *
     * @param $uploadDirectory
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(
        $uploadDirectory,
        EntityManagerInterface $entityManager
    ) {
        $this->uploadDirectory     = $uploadDirectory;
        $this->entityManager       = $entityManager;
    }

    /**
     * @return string
     */
    public function makeFile()
    {
        $fileMatchings = $this
            ->entityManager
            ->getRepository('CoreBundle:FileMatching')
            ->getAllApproved();

        $resultWriter = $this->convertToFile($fileMatchings);

        $newFileName = $this->newTempFileName . '.' . 'xlsx';
        $pathToFile  = $this->uploadDirectory . '/' . $newFileName;

        $resultWriter->save($pathToFile);

        return $pathToFile;
    }

    /**
     * @param array $fileMatchings
     * @return Xlsx
     */
    protected function convertToFile($fileMatchings)
    {
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        /** @var FileMatching $fileMatching */
        foreach ($fileMatchings as $key => $fileMatching) {
            $images = $fileMatching->getImages();
            $images->getValues();

            $imageIdsArray = [];
            /** @var Image $image */
            foreach ($images as $image) {
                $imageIdsArray[] = $image->getId();
            }
            $imageAsString = implode(';', $imageIdsArray);

            $sheet->setCellValueByColumnAndRow(1, $key + 1, $fileMatching->getTitle());
            $sheet->setCellValueByColumnAndRow(2, $key + 1, $imageAsString);
        }

        $writer = new Xlsx($spreadsheet);

        return $writer;
    }
}