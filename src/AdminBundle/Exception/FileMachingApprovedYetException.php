<?php

namespace AdminBundle\Exception;

use Throwable;

class FileMachingApprovedYetException extends \RuntimeException
{
    public function __construct($message, array $params = [], Throwable $previous = null)
    {
        parent::__construct($message, 500, $previous);
    }
}