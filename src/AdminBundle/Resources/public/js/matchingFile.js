$('.js-approve-button').on('click', function() {
    var itemId = $(this).data('id');
    var url    = $(this).data('url');

    var result = {};
    $('.js-image-' + itemId + ':checked').each(function(index, element) {
        result['i' + index] = $(element).data('id');
    });
    const params = jQuery.param(result);

    window.location.replace(url + '?' + params);
});

