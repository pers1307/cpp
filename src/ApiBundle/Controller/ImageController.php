<?php

namespace ApiBundle\Controller;

use CoreBundle\Entity\Image;
use CoreBundle\Exception\NotFoundImageException;
use CoreBundle\Exception\NotFoundRequireParameterException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Swagger\Annotations as SWG;

class ImageController extends AbstractFOSRestController
{
    /**
     * @SWG\Response(
     *     response=200,
     *     description="image full url",
     *     @SWG\Schema(
     *        type="object",
     *        example={"url": "full url to image"}
     *     )
     * )
     * @SWG\Response(
     *     response=423,
     *     description="Wrong input data",
     *     @SWG\Schema(
     *        type="object",
     *        example={"message": "text message"}
     *     )
     * )
     * @SWG\Response(
     *     response=500,
     *     description="Something went wrong",
     *     @SWG\Schema(
     *        type="object",
     *        example={"message": "Something went wrong"}
     *     )
     * )
     *
     * @SWG\Parameter(
     *     name="imageId",
     *     in="query",
     *     type="integer",
     *     description="Image id"
     * )
     *
     *
     * @SWG\Tag(name="image")
     *
     * @Rest\Get("/api/image")
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function getImageAction(Request $request)
    {
        try {
            $params = $request->query->all();

            if (!isset($params['imageId'])) {
                throw new NotFoundRequireParameterException(
                    'Not found imageId parameter'
                );
            }

            /** @var Image $image */
            $image = $this
                ->getDoctrine()
                ->getRepository('CoreBundle:Image')
                ->find($params['imageId']);

            if (is_null($image)) {
                throw new NotFoundImageException(
                    "Image with this id doesn't exists"
                );
            }

            $pathToImage = $request->getSchemeAndHttpHost() . '/' . $image->getPictureUrl();

            return new JsonResponse([
                'url' => $pathToImage
            ]);
        } catch (NotFoundImageException $exception) {
            return new JsonResponse([
                'message' => $exception->getMessage()
            ], 423);
        } catch (NotFoundRequireParameterException $exception) {
            return new JsonResponse([
                'message' => $exception->getMessage()
            ], 423);
        } catch (\Throwable $exception) {
            return new JsonResponse([
                'message' => 'Something went wrong'
            ], 500);
        }
    }
}

