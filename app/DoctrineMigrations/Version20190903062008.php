<?php declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190903062008 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE image DROP FOREIGN KEY FK_C53D045F7909E1ED');
        $this->addSql('CREATE TABLE file_matching (id INT AUTO_INCREMENT NOT NULL, title VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE file_matching_image (file_matching_id INT NOT NULL, image_id INT NOT NULL, INDEX IDX_D1B8C7E912388DA (file_matching_id), INDEX IDX_D1B8C7E3DA5256D (image_id), PRIMARY KEY(file_matching_id, image_id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE file_matching_image ADD CONSTRAINT FK_D1B8C7E912388DA FOREIGN KEY (file_matching_id) REFERENCES file_matching (id)');
        $this->addSql('ALTER TABLE file_matching_image ADD CONSTRAINT FK_D1B8C7E3DA5256D FOREIGN KEY (image_id) REFERENCES image (id)');
        $this->addSql('DROP TABLE research');
        $this->addSql('DROP INDEX IDX_C53D045F7909E1ED ON image');
        $this->addSql('ALTER TABLE image DROP research_id');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE file_matching_image DROP FOREIGN KEY FK_D1B8C7E912388DA');
        $this->addSql('CREATE TABLE research (id INT AUTO_INCREMENT NOT NULL, title VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('DROP TABLE file_matching');
        $this->addSql('DROP TABLE file_matching_image');
        $this->addSql('ALTER TABLE image ADD research_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE image ADD CONSTRAINT FK_C53D045F7909E1ED FOREIGN KEY (research_id) REFERENCES research (id)');
        $this->addSql('CREATE INDEX IDX_C53D045F7909E1ED ON image (research_id)');
    }
}
