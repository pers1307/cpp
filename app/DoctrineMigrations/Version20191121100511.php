<?php declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191121100511 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE file_matching_image DROP FOREIGN KEY FK_D1B8C7E3DA5256D');
        $this->addSql('ALTER TABLE file_matching_image DROP FOREIGN KEY FK_D1B8C7E912388DA');
        $this->addSql('ALTER TABLE file_matching_image ADD CONSTRAINT FK_D1B8C7E3DA5256D FOREIGN KEY (image_id) REFERENCES image (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE file_matching_image ADD CONSTRAINT FK_D1B8C7E912388DA FOREIGN KEY (file_matching_id) REFERENCES file_matching (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE file_matching_image DROP FOREIGN KEY FK_D1B8C7E912388DA');
        $this->addSql('ALTER TABLE file_matching_image DROP FOREIGN KEY FK_D1B8C7E3DA5256D');
        $this->addSql('ALTER TABLE file_matching_image ADD CONSTRAINT FK_D1B8C7E912388DA FOREIGN KEY (file_matching_id) REFERENCES file_matching (id)');
        $this->addSql('ALTER TABLE file_matching_image ADD CONSTRAINT FK_D1B8C7E3DA5256D FOREIGN KEY (image_id) REFERENCES image (id)');
    }
}
